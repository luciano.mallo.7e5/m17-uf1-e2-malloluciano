### Recrear una animació amb c# per consola.
Donat el següent codi:
https://gitlab.com/eruesga-xtec/protogameengine

---

## Repte 1: 
Hem de imprimir una matriu amb 0's. L'animació recrearà la pluja de caràcters de la pel·lícula Matrix. Una lletra aleatori caurà en cada frame una fila fins arribar al final. La columna on es generi també serà aleatori.

---

## Repte 2: (Bonus) 
Les lletres cauran per la columna que l'usuari ha seleccionat. L'usuari pot indicar la columna de sortida amb el numero de columna o amb un cursor mogut per fletxes.

---

### Format d'entrega: 
Enllaç de GITLAB amb el codi font
Recordeu afegir gitIgnore
No del projecte: M17-UF1-E2-CognomsNom
