﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameTools
{
    class Game : GameEngine
    {

        MyFirstProgram.MatrixRepresentation matrix = new MyFirstProgram.MatrixRepresentation(10, 20);
        Random rand = new Random();
        int[] columnToChange;

        public override void Start()
        {
            
            Console.CursorVisible = false;
            columnToChange = new int[matrix.TheMatrix.GetLength(1)];
            columnToChange[AskForPositiveEnter()] = 5;
            ChangePostitions();
            printMatriColors();
        }

        public int AskForPositiveEnter()
        {
            int number;

            do
            {
                Console.Clear();
                Console.WriteLine("\t\t Which column you to the rain start? ");
                number = Convert.ToInt32(Console.ReadLine());
            } while (number < 0 && number > matrix.TheMatrix.GetLength(1));
            return number;
        }

        public override void Update()
        {
            //Execution ontime secuence of every frame
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            NextColumn();
            ChangePostitions();
            //matrix.printMatrix(); //Change with the function below to use the method on the Matrix Representation Class
            printMatriColors(); //Functio to print the first char on white
        }

        protected override void Exit() {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Yellow;
            PrintCake();
            Console.ReadKey();

        }


        public void NextColumn()
        {
            for (int i = 0; i < columnToChange.Length; i++)
            {
                if (columnToChange[i] == 0)
                {
                    if (rand.Next(-80, 20) > 15)
                    {
                        columnToChange[i] = rand.Next(4, 10);
                    }
                }

            }
        }

        public void ChangePostitions()
        {
            for (int i = matrix.TheMatrix.GetLength(0) - 1; i > 0; i--)
            {
                for (int j = 0; j < matrix.TheMatrix.GetLength(1); j++)
                {
                    matrix.TheMatrix[i, j] = matrix.TheMatrix[i - 1, j];

                }
            }
            ControlColumns();
        }


        public void printMatriColors() 
        {
            Console.Clear();
            for (int x = 0; x < matrix.TheMatrix.GetLength(0); x++)
            {
                for (int y = 0; y < matrix.TheMatrix.GetLength(1); y++)
                {
                    if ( x + 1 < matrix.TheMatrix.GetLength(0)) 
                    { 
                    if (matrix.TheMatrix[x + 1, y] == ' ')
                    { 
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.Write($" {matrix.TheMatrix[x, y]}"); 
                    }

                    if (matrix.TheMatrix[x+1 , y ] != ' ')
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write($" {matrix.TheMatrix[x, y]}");
                    }
        
                }
                }

                Console.WriteLine();
            }
        }

        public void ControlColumns()
        {
            for (int i = 0; i < columnToChange.Length; i++)
            {
                if (columnToChange[i] > 0)
                {
                    matrix.TheMatrix[0, i] = RandomLetters();
                   
                    columnToChange[i]--;
                }
                if (columnToChange[i] == 0)
                {
                    matrix.TheMatrix[0, i] = ' ';
                }             

            }

        }

        public char RandomLetters()
        {
            int number = rand.Next(26);
            return (char)(((int)'A') + number);
        }




        public void PrintCake() 
        {

            Console.WriteLine("\t\t\t\n\n GAME OVER ");

            Console.WriteLine(@"                     /^\");
            Console.WriteLine(@"           /         (/^\)     /");
            Console.WriteLine(@"      \   ( \         \ /     ( \     /^\");
            Console.WriteLine(@"     / )   \ |        _|_      \ |   |/^\|");
            Console.WriteLine(@"    | /    _|_        | |      _|_    \ /");
            Console.WriteLine(@"    _|_    | |        | |      | |    _|_");
            Console.WriteLine(@"    | |    | |        | |      | |    | |");
            Console.WriteLine(@"    | |    | |    ****| |******| |    | |");
            Console.WriteLine(@"    | |****| |****    | |      | |****| |");
            Console.WriteLine(@"   *| |    | |                 | |    | |*****");
            Console.WriteLine(@" *  | |   T H E      C A K E          | |      *");
            Console.WriteLine(@"*               I S                             *");
            Console.WriteLine(@"| *            A     L I E !                  * |");
            Console.WriteLine(@"|  *****                                 *****  |");
            Console.WriteLine(@"|@      **********             **********      @|");
            Console.WriteLine(@"| @   @           *************           @   @ |");
            Console.WriteLine(@"|  @@@ @    @                       @    @ @@@  |");
            Console.WriteLine(@"|       @@@@ @      @       @      @ @@@@       |");
            Console.WriteLine(@" *            @@@@@@ @     @ @@@@@@            *");
            Console.WriteLine(@"  *                   @@@@@                   *");
            Console.WriteLine(@"   *****                                 *****");
            Console.WriteLine(@"        **********             **********");
            Console.WriteLine(@"                  *************");

        }

    }
}
        
